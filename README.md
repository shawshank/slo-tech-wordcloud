# What?
A simple tool to print out the word frequency of specific users on Slo-Tech forum, and create a wordcloud out of this frequency.   
It tries to purge non-relevant stuff like quotes, links, special chars, etc...   
The default minimum word length is 4, which can be changed.   
Also does some caching of posts to reduce the number of requests towards the forum.   

# Why?
Just bored, I guess. And refreshing my Python knowledge a bit.  
Any comments / critiques / suggestions welcome.

# How?
Couple of dependencies. You need Python 3 to run it and install WordCloud and BeautifulSoup modules.

```
pip3 install wordcloud
pip3 install beautifulsoup4
pip3 install requests
```

Then run it with either `--list` or `--wordcloud` args, together with your (or someone elses) user ID.

```
python3 main.py <user_ID> --wordcloud --list
```

The list gets stored in `wordlist` folder, and the wordcloud gets stored in `wordcloud` folder.

import re

def purge(post_content):
    """Removes quotes, user quotes, links, newlines, and special chars from a post"""
    post_content = purge_user_quotes(post_content)
    post_content = purge_quotes(post_content)
    post_content = purge_links(post_content)    
    post_content = purge_newline(post_content)
    post_content = purge_special_chars(post_content)
    return post_content

def purge_user_quotes(post_content):
    blocks = post_content.find_all("p", class_="user-quoted")
    purge_blocks(blocks)
    return post_content

def purge_quotes(post_content):
    blocks = post_content.find_all("blockquote")
    purge_blocks(blocks)
    return post_content

def purge_newline(post_content):
    blocks = post_content.find_all("br")
    purge_blocks(blocks)
    return post_content

def purge_links(post_content):
    blocks = post_content.find_all("a")
    purge_blocks(blocks)
    return post_content

def purge_special_chars(post_content):
    post_content = re.sub("[.,+-?!()%=\"'$€_@]", ' ', post_content.text)
    return post_content

def purge_blocks(blocks):
    for block in blocks:
        block.decompose()    
import requests, os, argparse
import utils.post_utils
from bs4 import BeautifulSoup
from collections import Counter
from wordcloud import WordCloud
import matplotlib.pyplot as plt

base_url = 'https://slo-tech.com/forum/'
min_word_length = 4 # Minimal length of the word
print_n = 50 # How many most common words to print
page_size = 50
word_list = Counter()
wordcloud_max_font_size = 30

def get_word_occurrence():
    if not os.path.exists(f"cache"):
        os.mkdir("cache")
    build_posts_cache()
    posts = get_cached_posts()
    for post in posts:
        post_content = post.find("div", class_="content")
        post_content = utils.post_utils.purge(post_content)
        for word in post_content.split():
            if len(word) >= min_word_length:
                word = word.lower()
                word_list[word] += 1
    return word_list

def build_posts_cache():

    if os.path.exists(f"cache/{user_id}"):
        print(f"[LOG] Found dir cache/{user_id}, assuming cache exists.")
    else:
        os.mkdir(f"cache/{user_id}")
        page_counter = 0
        response = requests.get(f"{base_url}c{user_id}/{page_counter}")

        while response.status_code == 200:
            response_content = response.text
            f = open(f"cache/{user_id}/{page_counter}_cache.txt", "w")
            f.write(response_content)
            f.close()

            page_counter += page_size
            response = requests.get(f"{base_url}c{user_id}/{page_counter}")

def get_cached_posts():
    posts = []
    for filename in os.listdir(f"cache/{user_id}"):
        f = open(f"cache/{user_id}/{filename}", "r")
        response_content = f.read()
        f.close()
        soup = BeautifulSoup(response_content, "html.parser")
        content = soup.find(id="content")
        posts += content.find_all("div", class_="post")    
    return posts

def create_wordlist(word_list):
    f = open(f"wordlist/{user_id}.txt", "w", encoding="utf-8")
    for word, counter in word_list.most_common():
        f.write(f"{word} : {counter}\n")
    f.close()
    print(f"[LOG] Wordlist generated at wordlist/{user_id}.txt")

def create_wordcloud(word_list):
    if not os.path.exists(f"wordcloud"):
        os.mkdir("wordcloud")
    wordcloud = WordCloud()
    wordcloud.generate_from_frequencies(frequencies=word_list)
    plt.figure()
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    plt.savefig(f"wordcloud/{user_id}.png")
    print(f"[LOG] Wordcloud generated at wordcloud/{user_id}.png")

parser = argparse.ArgumentParser()
parser.add_argument('user_id', type=str, help='User ID (e.g. 121979)')
parser.add_argument('--list', action='store_true', help='Create a list of most common words')
parser.add_argument('--wordcloud', action='store_true', help='Create and store a wordcloud')
args = parser.parse_args()
user_id = args.user_id

print(f"[LOG] Generating word counter for user {user_id}")
word_list = get_word_occurrence()
if args.list: 
    create_wordlist(word_list)
if args.wordcloud: 
    create_wordcloud(word_list)